pandocfilters==1.4.3
matplotlib==3.3.3
mypy==0.812
mypy-extensions==0.4.3
pdflatex==0.1.3
Pillow==8.1.0
flake8==3.8.4
flake8-polyfill==1.0.2
isort==5.6.4
packaging==20.4
pycodestyle==2.6.0
pylint==2.7.4
pytest==6.1.2
pytest-cov==2.10.1
pytest-datafiles==2.0

import pandoc_pyrun as pp

# --------------------------------------------
# 	 Ouput pandoc function tests
# --------------------------------------------

stringInput = "yo!"
id_block = ""


def test_all_out_funct_return_list_pandoc_object():
    none = pp.out_none()
    res = pp.out_res(id_block, stringInput)
    code = pp.out_code(id_block, stringInput)
    shell = pp.out_shell(id_block, stringInput)
    assert type(none) is list
    assert type(res) is list
    assert type(code) is list
    assert type(shell) is list


def start_test():
    test_all_out_funct_return_list_pandoc_object()
    pass


if __name__ == "__main__":
    start_test()
    pass

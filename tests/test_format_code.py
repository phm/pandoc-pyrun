import pandoc_pyrun as pp

# --------------------------------------------
# 	 Format input code tests
# --------------------------------------------


def test_format_code_separate_definition_of_code():
    stringInput = 'test1()\ndef hello(x):\n    print("yo")\n'
    res = pp.format_code(stringInput)
    assert len(res) == 2
    assert res[0] == "test1()\n"
    assert res[1] == 'def hello(x):\n    print("yo")\n'


def test_format_code_separate_definition_of_code_and_code():
    stringInput = (
        'hi()\ndef say_hello():\n    print("hi there\\nhow u doin?")\nsay_hello()'
    )
    res = pp.format_code(stringInput)
    assert len(res) == 3
    assert res[0] == "hi()\n"
    assert res[1] == 'def say_hello():\n    print("hi there\\nhow u doin?")\n'
    assert res[2] == "say_hello()\n"


def test_format_code_keep_in_definition():
    stringInput = 'def yo(x):\n    # still in def\n    print(e)\n    print(\\"\\"\\"helllo\n    \n    \n    you doing good?\\"\\"\\")\n'
    res = pp.format_code(stringInput)
    assert len(res) == 1


def test_format_code_ignore_empty_lines():
    stringInput = "yo()\nareUOk()\ndrinkSomeWater()\ngetSomeSleep()\n\n\n\nhopeUDoinWell(1,2,3,4)\n"
    res = pp.format_code(stringInput)
    assert len(res) == 5


def start_test():
    test_format_code_separate_definition_of_code_and_code()
    test_format_code_keep_in_definition()
    test_format_code_ignore_empty_lines()
    pass


if __name__ == "__main__":
    start_test()
    pass

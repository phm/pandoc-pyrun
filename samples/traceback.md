---
title: Traceback and rafters sample
author: TayebiDhulst
date: February 15, 2021
out: all
mode: prof
---

# Double Div, Error, rafters, .py, .python and .pandocPyrun

```{.pandocPyrun out=ALL}
1+1
```

```{.py out=ALL}
def salut(x):
    return x

>>> salut("yo")
>>> "" + 3
```
---
title: File sample
author: TayebiDhulst
date: February 15, 2021
pandoc_pyrun_out: ALL
PANDOC_PYRUN_MODE: prof
---

# File

```py
def f(X):
    return X
f(1)
```

```{.python file=tmp.py} 
f(1)
```
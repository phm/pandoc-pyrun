---
title: Scope's sample
author: TayebiDhulst
date: February 15, 2021
out: all
mode: student
---

# Scope

By default the *scope* option is set to **GLOBAL** and each function is stored in memory.

However, if we want to compile code in a *local* view and not add it to other code, we can use the *scope=LOCAL* option.

```{.python out=none}
def plusUn(x):
    return x+1
```

```{.python out=all scope=local}
def plusUn(x):
    return x+2
plusUn(2)
```

```{.python out=ALL}
plusUn(2)
```
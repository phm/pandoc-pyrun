# First sample

# Invisibility

```{.python Out=NoNe}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#that's a commentary
```

---

```{.python out=ALL}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#that's a commentary
```
---

```{.python out=code}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#that's a commentary
```

---

```{.python out=RES}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#that's a commentary
```

---

```{.py out=SHELL}
>>> couples(['a', 'b', 'c'])
>>> couples(['a', 'b', 'c'])
>>> couples(['a', 'b', 'c'])
>>> couples(['a', 'b', 'c'])
```

# Scope

By default the *scope* option is set to **GLOBAL** and each function is stored in memory.

However, if we want to compile code in a *local* view and not add it to other code, we can use the *scope=LOCAL* option.

```{.python out=none}
def plusUn(x):
    return x+1
```

```{.python out=all scope=local}
def plusUn(x):
    return x+2
plusUn(2)
```

```{.python out=ALL}
plusUn(2)
```

# File

```{.python out=ALL} 
def f(X):
    return X
f(1)
```

```{.python out=ALL file=tmp.py} 
f(1)
```

# Lesson example

## Python lesson

Make a predicate *for_all* parameterized by an iterable that returns **True** if and only if all elements of the iterable passed as a parameter are true and **False** otherwise.

Exemple :

```{.python out=NONE}
def for_all(liste):
    return not False in liste
```

```{.python out=ALL}
for_all([])
for_all((True, True, True))
for_all((True, False, True))
```


# Double Div, Error, rafters, .py, .python and .pandocPyrun

```{.pandocPyrun out=ALL}
1+1
```

```{.py out=ALL}
def salut(x):
    return x

>>> salut("yo")
>>> "" + 3
```

# Mathplot

```{.py out=ALL type=PLOT}
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()
plt.show()
```
# Turtle star

```{.python type=TURTLE out=RES}
import turtle

def spiral(n) :
    turtle.pencolor("red")
    for i in range(n):
        turtle.forward(i * 10)
        turtle.right(144)

spiral(20)
```

```{.python type=TURTLE out=NONE}
turtle.forward(160)
```

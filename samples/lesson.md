---
title: Lesson example
author: TayebiDhulst
date: February 15, 2021
out: ALL
mode: STUDENT
---

# Lesson example

## Python lesson

Make a predicate *for_all* parameterized by an iterable that returns **True** if and only if all elements of the iterable passed as a parameter are true and **False** otherwise.

Exemple :

```{.python out=NONE}
def for_all(liste):
    return not False in liste
```

```{.python out=ALL}
for_all([])
for_all((True, True, True))
for_all((True, False, True))
```
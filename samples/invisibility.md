---
title: Invisibility's sample
author: TayebiDhulst
date: February 15, 2021
out: ALL
---

# Invisibilité

```{.python out=NONE}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```

---

```{.python out=ALL}
def couples1(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```
---

```{.python out=CODE}
def couples2(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```

---

```{.python out=RES}
def couples3(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```

---

```{.py out=SHELL}
>>> couples(['a', 'b', 'c'])
>>> couples1(['a', 'b', 'c'])
>>> couples2(['a', 'b', 'c'])
>>> couples3(['a', 'b', 'c'])
```
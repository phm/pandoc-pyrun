### <ins>**23/01/21 -> 27/01/21 :**</ins>
* Lecture des filtres & de leurs codes  
* Renseignements sur leurs utilisations, exécutions,...  
* Tests d'exécution  

---

### <ins>**28/01/21 -> 03/02/21 :**</ins>
* <ins>***Jeudi 28/01 - Réunion n°1***</ins><br><br>  
* 1ère réunion (Zoom) en quatuor  
* Introduction du projet, des souhaits, des premières directives  
* Discussion sur Pandoc, le PJI,...  
* Initialisation du GitLab & de ce Cahier de Bord  
* Tâches à effectuer :  
  * Savoir comment écrire un filtre  
  * Lire un MD et repérer un codeblock  
  * Lancer un interprète Python  
  * Gérer les arguments  
* Relecture globale du fonctionnement des filtres Pandoc
* Commencement de création de filtres pandoc
* Exercices du site dans */exercice_filter*
* Conception de futures maquettes pour le possible code
* Premières fonctions de conversion/filtrage (gestion de l'attribut **out**)
* Avancée des fonctions de conversion/filtrage (gestion de l'attribut **range** & **file**)
* Les 3 attributs ci-dessus sont fonctionnels après tests sur input.md
---

### <ins>**04/02/2021 -> 08/02/2021 :**</ins>
* <ins>***Jeudi 04/02 - Réunion n°2***</ins><br><br>  
* Retours & tâches à effectuer :  
  * ">>>" à supprimer pour le retour RES *(allusion à interpréteur)* => faire une simple flèche ?  
  * Ne pas afficher les résultats exécution par exécution, considérer le CodeBlock comme UNE entité et tout exécuter/afficher à la suite du CODE  
  * Si ">>>" en début de code, "simulation d'un interpréteur", les ignorer et ne pas les afficher  
  * Insérer des métas pour les valeurs par défaut (attributs,...)  
  * Différencier CodeBlock CODE du CodeBlock RES  
  * Interpréteur reste vivant si on veut tout enchaîner, garde en mémoire "tout le bazar"  
* Remplacement des ">>>" du RES par un affichage en gras afin d'éviter une confusion avec l'interpréteur  
* Modifications sur la maquette  
* Tests des métas pandoc  
* Séparation CODE & EXEC via une fonction format_code  
* Interpréteur vivant en place et pas d'exécutions multiples  


---

### <ins>**09/02/2021 -> 17/02/2021 :**</ins>
* <ins>***Mardi 09/02 - Réunion n°3***</ins><br><br>  
* Retours & tâches à effectuer :  
  * Insérer out=STEP reprenant notre affichage de réponse d'avant  
  * Bien ignorer les >>> si entrés  
  * Bien séparer en DEUX BLOCS (un CODE et un ANSWER)  
  * Transition betterPython => py(thon) dans les maquettes  
  * Récupérer la sortie d'erreur et insérer dans l'affichage (et faire en sorte que ça reste en vie pour la console)  
  * Gérer les images (matplotlib, turtle rendu,...)  
  * Mode prof  
  * Docs, commentaires, exemples : maquette, attendu, obtenu (dans le dépôt)  
  * Réaliser un make pour génerer et choisir (pdf, md,...)  
* Séparation en DEUX BLOCS : une `<div class="answer">` qui contient une `<div class="sourceCode">` et une `<div class="res">`
* Sortie d'erreur récupérée et insérée dans l'affichage, dans la `<div class="res">`
* betterPython entre {} supprimé, betterPython et py SEULS acceptés (bug avec python)
* ">>>" ignorés lors de la compilation (ne pose pas de souci) mais reste affiché
* Insertion du out=STEP  
* Gestion des META  

---

### <ins>**18/02/2021 -> 01/03/2021 :**</ins>
* <ins>***Jeudi 18/02 - Réunion n°4***</ins><br><br>  
* Retours & tâches à effectuer :  
  * Bien retirer le pimp (gras, ...)  
  * Double div dans une grosse div  : .pyrun > .in et .out et .inout pour STEP  
  * Ne pas effacer à l'affichage les chevrons MAIS les ignorer lors de l'exécution  
  * Res doit être un codeblock de même et non du pimp MAIS GARDER LES DEUX CLASSES =/= (dans l'optique ou un 2nd filtre peut repasser dessus par exemple)  
  * Mettre python entre {} doit être possible et fichier doit être lisible si pandoc de base (pas notre filtre)  
  * "précipitation META", tester des cas d'utilisation avec les META,.... (produire une feuille élève, une feuille prof,..)  
* "pimp" retiré
* Bien répartis dans la double div souhaitée (.in, .out, .inout)
* Chevrons entrés affichés mais non-problématiques  
* Bien mis dans des codeblocks  
* out=PLOT permettant d'afficher en sortie le retour pictural d'un matplotlib  
* .python ajoutable dans les {} ce qui donne désormais .py, .python et .betterPython d'insérables entre {}  

---

### <ins>**02/03/2021 -> 15/03/2021 :**</ins>
* <ins>***Mardi 02/03 - Réunion n°5***</ins><br><br>  
* Retours & tâches à effectuer :  
  * chck -> check  
  * Proposer des exemples concrets (sujets de DS, mode prof,...) <=> Identifier des cas d'usage  
  * Rediriger vers une sortie "poubelle" l'affichage du show() pour plot pour pouvoir le laisser écrit  
  * Se servir du show() pour repérer si il y a une image à sortir ou non  
  * Revoir turtle <-> ghostscript  
* check corrigé
* BUG FORMAT_CODE => si \n en dur dans le code, souci de formattage  
* TURTLE A PEAUFINER (obliger un `import turtle et mot-clé pour afficher turtle`)  
* Début d'un diapo présentant différentes utilisations concrètes  
* Succès pour repérer un .show() déclenchant une sortie de l'image dans l'output (souci : CODE et RES possible mais pas ALL)  

---

### <ins>**16/03/2021 -> 24/03/2021 :**</ins>
* <ins>***Mardi 16/03 - Réunion n°6***</ins><br><br>  
* Retours & tâches à effectuer :  
  * Approfondir côté turtle (bye() ?, que fait done() ?,...)  
  * Cas d'usage : TD, Cours, DS et bien expliqué grâce à une doc utilisateur (comment faire ceci, cela,...)  
  * Le ALL du PLOT  
  * Le bug du format_code()  
  * Se repencher sur ce qu'impliquerait un mode ELEVE et un mode PROF  
* Refactor du code  
* Implémentation de l'attribut TYPE = {"PY", "PLOT", "TURTLE"}  
* Implémentation du MODE = "NEUTRAL" (-> tout à ALL SAUF les NONE qui restent à NONE)  
* Implémentation de moultes variables globales (pour les OUT, TYPE, RANGE et MODE)  
* Fix du bug de format_code() -> **si un utilisateur souhaite insérer un \\n dans son def, il devra écrire BACKSLASH_N**  
* Turtle désormais fonctionnel : type=TURTLE => **`import turtle` imposé** => **l'utilisateur devra utiliser SHOW_TURTLE et END_TURTLE**  
* **A NOTER : `out=RES` ou `out=ALL` avec un `type=PLOT` sans indiquer un `.show()` affichera une image d'erreur**  
* Cas d'usage bien entamé  
* Crop retours Turtle ?  

---

### <ins>**25/03/2021 -> 31/03/2021 :**</ins>
* <ins>***Jeudi 25/03 - Réunion n°7***</ins><br><br>  
* Retours & tâches à effectuer :  
  * **SHOW_TURTLE** & **END_TURTLE** à supprimer dans le retour  
  * Supprimer le **SHOW_TURTLE** MAIS SI `out=RES` ou `out=ALL`, on exécute le **SHOW_TURTLE** automatiquement à la fin du codeBlock  
  * Les \n contenus en brut dans le code => \\\n et non \n du coup trouver une autre méthode de formattage  
  * "doc_developpeur.md" => comment on a codé, etc.  
  * "doc_utilisateur.md" => "Vous êtes un novice et souhaitez faire X, dirigez-vous vers Y afin d'avoir un exemple" *(grimper en niveau de 'difficulté' à chaque niveau de maquette -> "tuto")*  
  * Remplacer .check par .pyrunPass  
  * "doc_users.md" => "manuel_reference.md"  
  * "PROF" => "DEBUG"  
  * "NEUTRAL" => "PROF"  
* .check -> .pyrunPass  
* NEUTRE -> PROF  
* PROF -> DEBUG  
* doc_users.md -> manuel_reference.md  
* BACKSLASH_N retiré du visuel utilisateur mais continue à être utilisé pour nous  
* SHOW_TURTLE & END_TURTLE supprimés & adaptés comme écrit ci-dessus  
* plus de bug dans format_code()  
* Crop retours Turtle ?  
* turtle.speed(0) retiré car générait un turtleWindow à chaque make : l'utilisateur devra indiqué lui-même sa speed  

---

### <ins>**03/04/2021 -> 07/04/2021 :**</ins>
* <ins>***Samedi 03/04 - Réponse Mail Récap Docs***</ins><br><br>  
* Retours & tâches à effectuer :  
  * user_manual -> format de tuto, progressif  
  * reference_manual -> + structuré, doit être exhaustif  
  * Préciser que la seule source attendue est Markdown  
  * RANGE -> SCOPE  
  * "trusted" -> expliquer que le code exécuté n'est pas vérifié au préalable avant exécution et doit être sécurisé, aux risques et périls de l'utilisateur  
  * Préciser les utilisations & fonctionnalités des variables OUT, TYPE,..  
  * Expliciter les indications de valeurs de classes (meta, entête,...) dans le reference_manual  
  * Crop retours Turtle ?  
* Tous les RANGE/range remplacés en SCOPE/scope  
* Définition de **trusted** insérée  
* Précision concernant la seule source attendue : Markdown  
* Précision concernant la fonctionnalité des classes MODE, OUT, TYPE, SCOPE, FILE  
* user_manual déjà en format de tuto & progressif, seul élément manquant : [TODO] de l'Installation  
* Ajout de 150 lignes dans le reference_manual afin d'accroître le côté exhaustif du document  
* Crop retours Turtle ?  

---

### <ins>**21/04/2021 -> 05/05/2021 :**</ins>
* <ins>***Mercredi 21/04 - Réunion n°8***</ins><br><br>  
* Retours & tâches à effectuer :  
  * Tutoriel "main dans la main" manquant -> juste milieu entre le trop synthétique du README et le trop développé du reference_manual  
  * Tableau dans le README => reference_manual  
  * Insérer les petites cases tout en haut du README vis à vis des tests  
  * Relire reference_manual (fautes,...)  
  * STEP -> SHELL  
  * Crop retours Turtle ?  
  * Tableau en HTML -> MD  
  * README éléments manquant : [TODO] de l'Installation  
* STEP -> SHELL  
* Tableau récap dans le README => Overview Table dans reference_manual  
* Fautes d'inattentions et erreurs corrigées dans le reference_manual  
* Insertion de la phrase dans spécifications.md dans "How to use it" du README  
* Modifications de la table récap (HTML -> MD)  
* Changement du 1er paragraphe du README suite au commit "first paragraph"  
* Commencement du tutorial, accompagné du tutorial_script  
* TODO pour installer pandoc_pyrun dans le README effectué  

---

### <ins>**11/05/2021 -> ??? :**</ins>
* <ins>***Mardi 11/05 - Réunion n°9 (en présentiel avec Mme PUPIN)***</ins><br><br>  
* Retours & tâches à effectuer :  
  * Ajouter éléments dans intro du reference_manual  
  * Check les tabulations des CodeBlock  
  * Nommage des classes pour le YAML  
  * Upper/Lower pour les classes  
  * Indiquer explicitement pour le speed(0) et l'affichage de la fenêtre turtle  
  * Utiliser un vrai fichier pour la commande du README indiquée  
  * Expliciter les utilisations des MODE  
* Ajout d'éléments dans l'intro du reference_manual  
* Tabulations des CodeBlock -> 4 espaces pour tous  
* Dans la commande du README, test.md -> hello.md  
* Tous les `<br>` en `\` désormais  
* Ajout d'une phrase d'explication pour chaque MODE existant  
* Indications explicites pour le turtle ajoutées dans le reference_manual  
* Possibilité d'écrire les classes et leurs valeurs en minuscules ET en MAJUSCULES ajoutée  
* Retraduction des maquettes  

---

### <ins>**INSÉRER DATE :**</ins>
* <ins>***... - Réunion n°.***</ins><br><br>  
* 
*  

---
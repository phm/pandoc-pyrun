Cahier des charges, spécifications
==================================

Vocabulaire, définitions
------------------------

* notion de _bloc de code_ Pandoc 
  - et de résultat produit (en PDF ou HTML) par un bloc de code

* possibles _classes_ et _attributs_ attachés un bloc de code

* on écrit par exemple le bloc de code suivant 

    ~~~~
    ``` {.python .numberLines startFrom="100"}
    def couples(l):
        t = []
        for i, e in enumerate(l) :
            t.append((i, e))
		return t
    
    couples(['a', 'b', 'c'])
    ```
    ~~~~

  `python` et `numberLines` sont des _classes_ ;
  `startFrom` est un attribut de _valeur_ `100`.

* le texte d'un bloc de code est produit « verbatim ». Il peut être mis en forme (_highlight_) pour mettre en évidence la syntaxe du langage.

Attendus
--------

Produire, pour un bloc de code, le texte du bloc et le résultat de l'exécution par un interprète Python de ce texte.

Plusieurs variantes sont attendues.

### Enchainement ###

Il s'agit de faire en sorte, ou non, que l'exécution de l'interprète Python soit

* **globale** : les exécutions de tous les blocs de code sont réalisées par la même instance d'exécution de l'interprète, ou
* **locale** : chaque bloc de code est exécuté dans une instance partoculière d'exécution de l'interprète.

### Invisibilité ###

Le bloc de code et le résultat de son exécution doivent pouvoir ne pas être affichés.

Ce choix doit pouvoir se faire indépendement pour le bloc de code et le résultat. 

Ce choix doit pouvoir se faire bloc de code par bloc de code. 

Pour un bloc donné, il sera donc possible - tout en exécutant le code, en particulier dans le cadre d'un enchaînement - 

* d'afficher le code et le résultat d'exécution,
* d'afficher le code seul,
* d'afficher le résultat seul,
* d'afficher le résultat ligne par ligne,
* de ne rien afficher.

### Fichier ###

Il dit être possible de lire le texte d'un boc de code depuis un fichier externe, a priori `.py`.

### Chevrons ###

Le bloc de code devra être considéré comme un interprètre vivant, en utilisant `>>>` comme si c'était une ligne de commande `python`.

### Erreurs ###

Les erreurs devront être affichées et mis en forme lorsque l'on met un code défaillant dans le bloc de code.

### Matplotlib ###

La librairie `matplotlib` qui génère des graphes devra être comprise par les blocs de codes et son résultat devra être affichable en image.

Un graphe devra être mis en mémoire, et affichable dans tous les autres blocs de codes.

### Mode professeur ###

Le mode professeur donnera accès à tous les codes ainsi qu'à tous les résultats du fichier créé, qu'il soit visible ou non.

### Turtle ###

La libraire `turtle` qui permet la création d'une tortue graphique pour initier les enfants au monde de la programmation.

Celle-ci devra pouvoir être utilisé dans les blocs de codes et son résultat est affiché en image dans le résultat.

<!-- eof -->


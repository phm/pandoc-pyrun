# pando-pyrun

_View execution of Python code in Pandoc_.

First year projet master informatique, 2020/21.

* [specifications](SPECIFICATIONS.md) 

* [logbook](logbook.md)


# Authors

- TAYEBI Ajwad
- D'HULST Thomas

# Supervisors

- Mme PUPIN Maude
- Mr MARQUET Philippe 

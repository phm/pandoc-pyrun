# pandoc_pyrun

## Reference manual

This filter is used to execute **Python** code embedded in Markdown CodeBlocks. The result of the execution is displayed in the
produced documents, for example in PDF or HTML format. Options allow to display the Python code and / or the result of its execution.  
Create a regular markdown code block and put your *trusted* code inside and the result will be a new Markdown *Div* with a class *.pyrun*. Inside the Markdown *Div*, the output will change depending on the options given. There are 2 optionals class. All input code will be compiled and executed, except if you put the *.pyrunPass* class.  

**Not compiled** : 
```
    ```{.python .pyrunPass}
    for i in range(2*6*62*54*65**6*64*2653):
        print(i)
    ```
```

***

## Summary

- [pandoc_pyrun](#pandoc_pyrun)
  - [Reference manual](#reference-manual)
  - [Summary](#summary)
  - [Overview Table](#overview-table)
  - [Classes](#classes)
    - [**out** :](#out-)
    - [**type** :](#type-)
    - [**scope** :](#scope-)
    - [**file** :](#file-)
  - [Meta](#meta)
    - [Classes in Meta](#classes-in-meta)
    - [Mode](#mode)
    - [Default](#default)


***

## Overview Table

\
All the classes' names and values can be written in both ways : lower case and upper case.  
\

|         | **OUT** - _sets the out value for the CodeBlock, the display of the result of the filter execution_     |
|---------|---------------------------------------------------------------------------------------------------------|
| _value_ | _description_                                                                                           |
| `CODE`  | Only prints the code of the CodeBlock. **[default]**                                                    |
| `RES`   | Only prints the result of the execution of the CodeBlock's code.                                        |
| `ALL`   | Prints the code and the result of the execution of the CodeBlock's code.                                |
| `SHELL` | Only prints the result of the execution of the code, line by line, in the CodeBlock, as done in a Shell |
| `NONE`  | Prints nothing.                                                                                         |

|          |  **TYPE** - _sets the type value for the CodeBlock, allowing special outputs for two libraries_             |
|----------|-------------------------------------------------------------------------------------------------------------|
|  _value_ | _description_                                                                                               |
| `PY`     | The code is interpreted as Python, without special output. **[default]**                                    |
| `PLOT`   | The Python code contains matplotlib instructions and will output plots. **[only in RES or ALL out mode]**   |
| `TURTLE` | The Python code contains `turtle` instructions and will ouptupt drawings. **[only in RES or ALL out mode]** |


|          |  **SCOPE** - _sets the scope value for the CodeBlock, making lines's compilation temporary or not_     |
|----------|--------------------------------------------------------------------------------------------------------|
| _value_  | _description_                                                                                          |
| `GLOBAL` | The code is compiled globally, which means from it's compilation until the filter's end. **[default]** |
| `LOCAL`  | The code is compiled locally, which means only for the specific CodeBlock.                             |


|         |  **FILE** - _sets the file value for the CodeBlock, enabling file import_ |
|---------|---------------------------------------------------------------------------|
| _value_ | _description_                                                             |
| `???.py`| The script of the Python file is implemented in the actual CodeBlock.     |


|           | **MODE** - _sets the global mode for the file, affects output mode_          |
|-----------|------------------------------------------------------------------------------|
| _value_   | _description_                                                                |
| `STUDENT` | The OUT values are respected according to the writer's choice. **[default]** |
| `PROF`    | The OUT values are all forced to ALL value, except for NONE ones.            |
| `DEBUG`   | The OUT values are all forced to ALL value.                                  |

\

***

## Classes

### **out** :

**- CODE**

Use this to compile, execute and display the code. The output will be a Markdown *Div* with a class *.in*.  

__Example__:
```
    ```{.python out=CODE}
    def roundMe(a,b):
        print(round(a,b))

    tax = 12.5 / 100
    price = 100.50
    price * tax
    price + _
    roundMe(_, 2)
    ```
```

become:

```
::: {.pyrun}
::: {.in}
    ``` {.python .pyrunPass}
    def roundMe(a,b):
        print(round(a,b))
        
    tax = 12.5 / 100
    price = 100.50
    price * tax
    price + _
    roundMe(_, 2)
    ```
:::
:::
```

**- RES**

Use this when you want your code to be compiled, executed and only the result is displayed. The output will be a Markdown *Div* with a class *.out*.  

 __Example__:
```
    ```{.python out=RES}
    def roundMe(a,b):
        print(round(a,b)

    tax = 12.5 / 100
    price = 100.50
    price * tax
    price + _
    roundMe(_, 2)
    ```
```

become:

```
::: {.pyrun}
::: {.out}
    ``` {.python .pyrunPass}
    12.5625
    113.0625
    113.06
    ```
:::
:::
```

**- NONE**

Use this when you want your code to be compiled, executed but nothing has to be displayed.  

 __Example__:
```
    ```{.python out=NONE}
    def roundMe(a,b):
        print(round(a,b))

    tax = 12.5 / 100
    price = 100.50
    price * tax
    price + _
    roundMe(_, 2)
    ```
```

become nothing at all but the given code is compiled.


**- ALL**

Use this when you want your code to be compiled, executed and all the content will be displayed. The output will be two *Div's*, first with a class *.in* and the second with *.out*. It is a mix of **CODE** and **RES**.  

 __Example__:
```
    ```{.python out=ALL}
    def roundMe(a,b):
        print(round(a,b))

    tax = 12.5 / 100
    price = 100.50
    price * tax
    price + _
    roundMe(_, 2)
    ```
```

become:

```
::: {.pyrun}
::: {.in}
    ``` {.python .pyrunPass}
    def roundMe(a,b):
        round(a,b)

    tax = 12.5 / 100
    price = 100.50
    price * tax
    price + _
    roundMe(_, 2)
    ```
:::

::: {.out}
    ``` {.python .pyrunPass}
    12.5625
    113.0625
    113.06
    ```
:::
:::
```


**- SHELL**

Use this when you want your code to be compiled, executed and all the content will be displayed. But the result will match the line that calls it. The output will be a Markdown *Div* with a class *.inout*.  

 __Example__:
```
    ```{.python out=SHELL}
    def roundMe(a,b):
        print(round(a,b))

    tax = 12.5 / 100
    price = 100.50
    price * tax
    price + _
    roundMe(_, 2)
    ```
```

become:

```
::: {.pyrun}
::: {.inout}
    ```{.python .pyrunPass}
    def roundMe(a,b):
        print(round(a,b))
    tax = 12.5 / 100
    price = 100.50
    price * tax
    12.5625
    price + _
    113.0625
    roundMe(_, 2)
    113.06
    ```
:::
:::
```

### **type** :


**- PY**

Use this when you want to execute simple python code.  

**- PLOT**

Use this when you want to execute python code using [**mathplot**](https://matplotlib.org/) library. **Don't forget to import the library!**  

 __Example__:
```
    ```{.python out=ALL type=PLOT}
    #code plot...
    ```
```

become:

```
::: {.pyrun}
::: {.in}
    ``` {.python .pyrunPass}
    # code plot ..
    ```
:::

::: {.out}
::: {.plot}
![](cb5c56e03969f70cf0666495b597e7ef701a2ccb.png)
#generated image from code plot
:::
:::

:::
```

**- TURTLE**

Use this when you want to execute python code using [**turtle**](https://docs.python.org/fr/3/library/turtle.html) library. *You don't need to import the library.*  
A window will be opened when you will wait for an output containing a turtle plot. To make it closed faster, you can add a specification like `turtle.speed(0)` to put the speed of the turtle at his maximum.  

__Example__:
```
    ```{.python out=ALL type=TURTLE}
    #code turtle...
    ```
```

become:

```
::: {.pyrun}
::: {.in}
    ``` {.python .pyrunPass}
    turtle.forward(160)
    ```
:::

::: {.turtle}
![](a823f381c16c511db264e9ad7a9976592510f258.png)
#generated image from turtle
:::
:::
```

### **scope** :

**- GLOBAL**

Use this when you want to execute python code and keep the scope *global*.  

All the python code will be compiled, kept in cache and accessible by all others `CodeBlock`.  

__Example__:
```
    ```{.python out=NONE scope=GLOBAL}
    def bonjour():
        print("Salut toi!!!")
    ```

    ```{.python out=RES}
    bonjour()
    ```
```

become:

```
::: {.pyrun}
::: {.out}
    ``` {.python .pyrunPass}
    Salut toi!!!
    ```
:::
:::
```

**- LOCAL**:

Use this when you want to execute python code and keep the scope *local*.   

All the python code will be compiled but not kept in cache and they will not be accessible by all others `CodeBlock`.  

__Example__:
```
    ```{.python out=NONE scope=LOCAL}
    def bonjour():
        print("Salut toi!!!")
    ```

    ```{.python out=RES}
    bonjour()
    ```
```

become:

```
::: {.pyrun}
::: {.out}
    ``` {.python .pyrunPass}
    Traceback (most recent call last):
    File "<console>", line 1, in <module>
    NameError: name 'bonjour' is not defined
    ```
:::
:::
```

### **file** :

Use this class when you want to integrate a python code file into the `CodeBlock`.  

Imagine a python file named **tmp.py**.
```python
#in the file tmp.py
def bonjour():
    print("that works just fine! :)")
```

__Example__:
```
    ```{.python out=ALL file=tmp.py}
    bonjour()
    ```
```

become:

```
::: {.pyrun}
::: {.in}
    ``` {.python .pyrunPass}
    def bonjour():
        print("that works just fine! :)")
    bonjour()
    ```
:::

::: {.out}
    ``` {.python .pyrunPass}
    that works just fine! :)
    ```
:::
:::
```

*Tips* : if you want to import a python code file but not show it, just use **out=NONE**.  

Like ` ```{.python file=FILENAME.py out=NONE}``` `


***

## Meta

### Classes in Meta

You can set all classes (except *file*) described above inside the `Meta` Markdown just like that :  

```
---
title: Python Lesson 1
pandoc_pyrun_out: ALL
pandoc_pyrun_type: PY
pandoc_pyrun_scope: LOCAL
---
```

### Mode

We add a new `Meta` keyword : **pandoc_pyrun_mode** that has three different values.  

```
---
pandoc_pyrun_mode: STUDENT/PROF/DEBUG
---
```

**- STUDENT**:

This is the default mode. All `CodeBlock`'s output will match their *out* value.  
Used generally for a STUDENT use (an exam subject, some exercises,...).  

**- PROF**:

All *out* values will be set on **ALL**, except if a **NONE** value is set on the `CodeBlock`.  
So the output will always be : the code, then the result except on CodeBlock like ` ```{.py out=NONE}``` `.  
Used generally for a PROFESSOR use or deploy (show the subject to the other teachers,...).  

**- DEBUG**:

All *out* values will be set on **ALL**, no matter what *out* value is, for each `CodeBlock`.  
Used generally for a DEBUG use (all displayed, easy to find possible errors).  

### Default

All classes are optional and these are the default options:

- Default *out* value = `CODE`     (*the output will be the code*)
- Default *mode* value = `STUDENT` (*the result will match out class value*)
- Default *scope* value = `GLOBAL` (*all the code will be accessible by all Codeblock*)
- Default *type* value = `PY`      (*the code executed is Python code*)

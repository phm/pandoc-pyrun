---
title: Some University Exam Exercises
author: Your name
date: April 24, 2021
pandoc_pyrun_mode: STUDENT
pandoc_pyrun_out : CODE
pandoc_pyrun_type : PY
---



```{.py out=NONE}
import random
import os
import sys
import turtle
```



> **Exercise 1 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mark is a young boy who wants to organize a birthday with his friends and he wants to suggest an original game to them. Here are the rules :  
> - They all pick a number from 0 to 100 *[a number can't be picked more than once]*  
> - In a jar, we put one paper per number, this number written on it  
> - We draw one paper per one paper :  
>   - If the drawn number has been chosen by someone, (s)he loses the game  
>   - If it's not, we do nothing and keep drawing   
> - When there are two persons remaining, we make a coin toss to determine the winner   
> 
> **NB :** They have to be at least 2 to play this game.  
>   
> **Question 1 :** *Write a function `tossCoin()` that returns 0 (for heads) or 1 (for tails).*  

```{.py out=NONE}
def tossCoin():
    return random.randint(0,1)
```
```{.py out=SHELL}
>>> [tossCoin() for i in range(15)]
```
 


> **Question 2 :** *Write a function `checkElement(elt, lst)` that returns True if elt is in lst, False otherwise.*  

```{.py out=NONE}
def checkElement(elt, lst):
    return elt in lst
```
```{.py out=SHELL}
>>> checkElement(1, [1,2,3,4])
>>> checkElement("a", ["n", "c", "b"])
>>> checkElement(True, [False, False, not False, False])
```



> **Question 3 :** *Write a function `removeElement(elt, lst)` that returns lst, with elt removed if contained in it, unchanged otherwise.*  

```{.py out=NONE}
def removeElement(elt, lst):
    if checkElement(elt, lst):
        lst.remove(elt)
    return lst
```
```{.py out=SHELL}
>>> removeElement(1, [1,2,3,4])
>>> removeElement("a", ["n", "c", "b"])
>>> removeElement(True, [False, False, not False, False])
>>> removeElement(5, [5, 10, 15, 5, 30])
```



> **Question 4 :** *Write a function `popElement(lst)` that returns a random element from lst and lst without this random element.*  

```{.py out=NONE}
def popElement(lst):
    i = random.randint(0, len(lst) - 1)
    return lst.pop(i), lst
```
```{.py out=SHELL}
>>> popElement([1,2,3,4])
>>> popElement(["n", "c", "b"])
```



> **Question 5 :** *Write a function `virtuosoRoulette(players)` that takes the list of all the chosen numbers and returns the one that has not been eliminated, -1 if something is wrong.*  

```{.py out=NONE}
def virtuosoRoulette(players):
    if len(players) < 2 or not all((type(nbr) == int) and (0 <= nbr <= 100) for nbr in players): return -1
    
    nbrs = [i for i in range(101)]
    while len(players) > 2 :
        elt, nbrs = popElement(nbrs)
        players = removeElement(elt, players)
        
    return players[0] if tossCoin() == 0 else players[1]
```
```{.py out=SHELL}
>>> virtuosoRoulette([55, 15, 67, 99, 1, 5])
>>> virtuosoRoulette([55, 15, 67, "99", 1, 5])
>>> virtuosoRoulette([55, 15, 670, 99, 1, 5])
```





> **Exercise 2 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Below, you'll find a turtle trace with the associated code.      
```{.py out=ALL type=TURTLE}
def hexagon():
    turtle.speed(0)
    turtle.pencolor("purple")
    for i in range(6):
        turtle.forward(90)
        turtle.right(60)

hexagon()
``` 
> **Question 1 :** *Write a function `cross()` that, starting from the trace above, will give you this trace.*  
```{.py out=NONE}
def cross():
    turtle.speed(0)
    turtle.right(60)
    turtle.forward(180)
    turtle.backward(180)
    
    turtle.left(60)
    turtle.forward(90)
    
    turtle.right(120)
    turtle.forward(180)
    turtle.backward(180)
    
    turtle.left(60)
    turtle.forward(90)
    
    turtle.right(120)
    turtle.forward(180)
    turtle.backward(180)
```
```{.py out=ALL type=TURTLE}
cross()
```




 
> **Exercise 3 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proove that your previous function `tossCoin()` is right by showing multiple results of its execution in a plot.
> The expected plot should look like this :   
```{.py out=RES type=PLOT}
import matplotlib
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
lst = [tossCoin() for i in range(1000)]
occu = [lst.count(0), lst.count(1)]
nbrs = ['0', '1']
ax.bar(nbrs, occu)
plt.show()
```